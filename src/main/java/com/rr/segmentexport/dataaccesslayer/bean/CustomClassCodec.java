package com.rr.segmentexport.dataaccesslayer.bean;

import java.nio.ByteBuffer;

import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.UDTValue;
import com.datastax.driver.core.UserType;
import com.datastax.driver.core.exceptions.InvalidTypeException;

public class CustomClassCodec extends TypeCodec<CustomClass> {

  private final TypeCodec<UDTValue> innerCodec;

  private final UserType userType;

  public CustomClassCodec(TypeCodec<UDTValue> innerCodec, Class<CustomClass> javaType) {
    super(innerCodec.getCqlType(), javaType);
    this.innerCodec = innerCodec;
    this.userType = (UserType)innerCodec.getCqlType();
  }

  @Override
  public ByteBuffer serialize(CustomClass value, ProtocolVersion protocolVersion) throws InvalidTypeException {
    return innerCodec.serialize(toUDTValue(value), protocolVersion);
  }

  @Override
  public CustomClass deserialize(ByteBuffer bytes, ProtocolVersion protocolVersion) throws InvalidTypeException {
    return toCustomClass(innerCodec.deserialize(bytes, protocolVersion));
  }

  @Override
  public CustomClass parse(String value) throws InvalidTypeException {
    return value == null || value.isEmpty() ? null : toCustomClass(innerCodec.parse(value));
  }

  @Override
  public String format(CustomClass value) throws InvalidTypeException {
    return value == null ? null : innerCodec.format(toUDTValue(value));
  }

  protected CustomClass toCustomClass(UDTValue value) {
    if (value == null) return null;

    CustomClass cc = new CustomClass();
    cc.setCustomName("custom_name");
    cc.setCustomName2("custom_name_2");
    return cc;
  }

  protected UDTValue toUDTValue(CustomClass value) {
    return value == null ? null : userType.newValue()
      .setString("custom_name", value.getCustomName())
      .setString("custom_name_2", value.getCustomName2());
  }
}
