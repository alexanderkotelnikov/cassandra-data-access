package com.rr.segmentexport.dataaccesslayer.bean;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.datastax.driver.mapping.annotations.Frozen;
import com.datastax.driver.mapping.annotations.FrozenValue;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;

/**
 * Mapping docs
 *
 * <a href="https://github.com/datastax/java-driver/tree/3.x/manual/object_mapper/creating">docs</a>
 */


/*
  CREATE TABLE ks.multiple (
    id int,
    siteid text,
    customclasslist list<frozen<custom_class>>,
    names set<text>,
    map map<int, frozen<list<int>>>,
    PRIMARY KEY (id, siteid)
  ) WITH default_time_to_live = 100000;  //seconds
 */


//  id | siteid  | customclasslist                                                    | map                    | names
//   ----+---------+--------------------------------------------------------------------+------------------------+--------------------
//   1 | site_id | [{custom_name: 'custom name', custom_name_2: 'other custom name'}] | {1: [1, 2], 2: [3, 4]} | {'name1', 'name2'}


@Table(keyspace = "ks", name = "multiple")
public class MultipleKeyed {

  @PartitionKey(0)
  private int id;

  @PartitionKey(1)
  private String siteId;

  private Set<String> names;

  @Frozen
  private List<CustomClass> customClassList;

  @FrozenValue
  private Map<Integer, List<Integer>> map;

  public Set<String> getNames() {
    return names;
  }

  public MultipleKeyed setNames(Set<String> names) {
    this.names = names;
    return this;
  }

  public int getId() {
    return id;
  }

  public MultipleKeyed setId(int id) {
    this.id = id;
    return this;
  }

  public String getSiteId() {
    return siteId;
  }

  public MultipleKeyed setSiteId(String siteId) {
    this.siteId = siteId;
    return this;
  }

  public List<CustomClass> getCustomClassList() {
    return customClassList;
  }

  public MultipleKeyed setCustomClassList(List<CustomClass> customClassList) {
    this.customClassList = customClassList;
    return this;
  }

  public Map<Integer, List<Integer>> getMap() {
    return map;
  }

  public MultipleKeyed setMap(Map<Integer, List<Integer>> map) {
    this.map = map;
    return this;
  }

}
