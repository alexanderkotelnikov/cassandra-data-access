package com.rr.segmentexport.dataaccesslayer.bean;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Computed;
import com.datastax.driver.mapping.annotations.Frozen;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;

/**
 * Mapping docs
 *
 * <a href="https://github.com/datastax/java-driver/tree/3.x/manual/object_mapper/creating">docs</a>
 */


/*
  CREATE TABLE ks.single (
    id int PRIMARY KEY,
    name text,
    name_2 text,
    custom_class frozen<custom_class>
  ) WITH default_time_to_live = 100000;  //seconds

   CREATE INDEX ON ks.single (custom_class); // to use predicate on non-id columns index is required
 */

//  id | custom_class                                                     | name
//   ----+------------------------------------------------------------------+------
//   1 | {custom_name: 'custom name', custom_name_2: 'other custom name'} | name1

@Table(keyspace = "ks", name = "single")
public class SingleKeyed {

  @PartitionKey
  private int id;
  private String name;

  // column name does not match field name
  @Column(name = "custom_class")
  // Will be mapped as a 'frozen<list<text>>' (Currently this is only for informational purposes as mapper doesn't support
  // frozen serialization).
  // A frozen value serializes multiple components into a single value.
  // Non-frozen types allow updates to individual fields. Cassandra treats the value of a frozen type as a blob.
  // The entire value must be overwritten.
  @Frozen
  private CustomClass customClass;

  // result of a computation on the Cassandra side
  @Computed("ttl(name)") // ttl function
  private int ttl;


  public int getId() {
    return id;
  }

  public SingleKeyed setId(int id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public SingleKeyed setName(String name) {
    this.name = name;
    return this;
  }

  public CustomClass getCustomClass() {
    return customClass;
  }

  public SingleKeyed setCustomClass(CustomClass customClass) {
    this.customClass = customClass;
    return this;
  }

  public int getTtl() {
    return ttl;
  }

  public SingleKeyed setTtl(int ttl) {
    this.ttl = ttl;
    return this;
  }

}
