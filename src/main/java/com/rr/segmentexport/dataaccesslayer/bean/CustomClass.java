package com.rr.segmentexport.dataaccesslayer.bean;

import com.datastax.driver.mapping.annotations.Field;
import com.datastax.driver.mapping.annotations.UDT;



// CREATE TYPE ks.custom_class ( custom_name text, custom_name_2 text );

@UDT(keyspace = "ks", name = "custom_class")
public class CustomClass {

  // To declare a different name or use case-sensitivity, use the @Field annotation for UDT classes
  @Field(name = "custom_name")
  private String customName;

  @Field(name = "custom_name_2")
  private String customName2;


  public CustomClass() {}

  public CustomClass(String customName, String customName2) {
    this.customName = customName;
    this.customName2 = customName2;
  }


  public String getCustomName2() {
    return customName2;
  }

  public void setCustomName2(String customName2) {
    this.customName2 = customName2;
  }

  public String getCustomName() {
    return customName;
  }

  public void setCustomName(String customName) {
    this.customName = customName;
  }

}
