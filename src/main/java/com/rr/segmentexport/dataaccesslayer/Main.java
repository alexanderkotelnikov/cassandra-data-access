package com.rr.segmentexport.dataaccesslayer;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.select;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.CodecRegistry;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.UDTValue;
import com.datastax.driver.core.UserType;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.Result;
import com.rr.segmentexport.dataaccesslayer.bean.CustomClass;
import com.rr.segmentexport.dataaccesslayer.bean.CustomClassCodec;
import com.rr.segmentexport.dataaccesslayer.bean.MultipleKeyed;
import com.rr.segmentexport.dataaccesslayer.bean.SingleKeyed;

/**
 * docs
 * <a href="https://github.com/datastax/java-driver">docs</a>
 */
public class Main {

  public static void main(String[] args) {
    CodecRegistry codecRegistry = new CodecRegistry(); // for registering custom codecs

    Cluster cluster = Cluster.builder()
      // .withPort(9042) // 9042 is default
      // .withClusterName("myCluster")
      .withCodecRegistry(codecRegistry)
      .addContactPoint("127.0.0.1")
      .build();


    // CREATE KEYSPACE ks WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 3};
    // docs <a href="http://cassandra.apache.org/doc/latest/cql/"></a>
    Session session = cluster.connect("ks"); // keyspace 'ks'
    MappingManager manager = new MappingManager(session);

    singleKeyed(manager);
    // multipleKeyed(manager);
    // sqlLike(session, codecRegistry);

    cluster.close();
  }

  private static void singleKeyed(MappingManager manager) {
    Mapper<SingleKeyed> mapper = manager.mapper(SingleKeyed.class);

    final int id = 1;

    SingleKeyed entity = new SingleKeyed()
      .setId(id)
      .setName("name")
      .setCustomClass(new CustomClass("custom name", "other custom name"));

    // save
    mapper.save(entity);

    // get by id
    SingleKeyed other = mapper.get(id);

    // delete by id
    mapper.delete(id);

    // delete by entity
    mapper.save(other);
    mapper.delete(other);
  }

  private static void multipleKeyed(MappingManager manager) {
    Mapper<MultipleKeyed> mapper = manager.mapper(MultipleKeyed.class);

    final int id = 1;
    final String siteId = "site_id";

    Map<Integer, List<Integer>> map = new HashMap<>();
    map.put(1, Arrays.asList(1, 2));
    map.put(2, Arrays.asList(3, 4));

    MultipleKeyed entity = new MultipleKeyed()
      .setId(id)
      .setSiteId(siteId)
      .setCustomClassList(Arrays.asList(new CustomClass("custom name", "other custom name")))
      .setNames(newHashSet("name1", "name2"))
      .setMap(map);

    // save
    mapper.save(entity);

    // get by id
    MultipleKeyed other = mapper.get(id, siteId);

    // delete by id
    mapper.delete(id, siteId);

    // delete by entity
    mapper.save(other);
    mapper.delete(other);
  }

  private static void sqlLike(Session session, CodecRegistry codecRegistry) {
    // insert
    session.execute("INSERT INTO ks.single (id, name, custom_class)"
      + "VALUES (2, 'name1', {custom_name: 'custom name', custom_name_2: 'other custom name'});");

    // select
    ResultSet rs = session.execute("SELECT * FROM ks.single "
      + "WHERE custom_class = {custom_name: 'custom name', custom_name_2: 'other custom name'};");

    // custom codec to get custom types from result set
    UserType ccType = session.getCluster().getMetadata().getKeyspace("ks").getUserType("custom_class");
    TypeCodec<UDTValue> ccTypeCodec = codecRegistry.codecFor(ccType);
    CustomClassCodec ccCodec = new CustomClassCodec(ccTypeCodec, CustomClass.class);
    codecRegistry.register(ccCodec);

    rs.forEach(row -> {
      int id = row.getInt("id");
      String name = row.getString("name");
      CustomClass cc = row.get("custom_class", CustomClass.class);
    });



    // select with mapping
    rs = session.execute("SELECT * FROM ks.single "
      + "WHERE custom_class = {custom_name: 'custom name', custom_name_2: 'other custom name'};");

    MappingManager manager = new MappingManager(session);
    Mapper<SingleKeyed> mapper = manager.mapper(SingleKeyed.class);

    Result<SingleKeyed> list = mapper.map(rs);
    list.forEach(sk -> sk.getId());


    // select with query builder
    rs = session.execute(select()
      .from("ks", "single")
      .where(eq("id", 2))
      .getQueryString());

    rs.forEach(row -> {
      int id = row.getInt("id");
      String name = row.getString("name");
      CustomClass cc = row.get("custom_class", CustomClass.class);
    });

    // delete
    session.execute("DELETE FROM ks.single WHERE id = 2;");
  }

}
